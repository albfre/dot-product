CC = clang++
CFLAGS = -g -std=c++17 -O2 -ffast-math -ftree-vectorize -Wall -pedantic -mavx -mfma
DEBUGFLAGS = -g -std=c++17 -O2 -ftree-vectorize -fopt-info-vec-missed -Wall -pedantic
FILE = blas1
SRCS = $(FILE).h
OBJS = $(FILE).o
TEST = test
TESTSRC = $(TEST).cpp

all: 

product: $(SRCS) \
; $(CC) $(CFLAGS) -c $(SRCS)

test: $(TESTSRC) \
; $(CC) $(CFLAGS) -o $(TEST) $(TESTSRC)

clean: \
; $(RM) $(OBJS) $(TEST)

write: \
; echo $(OBJS)
