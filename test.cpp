#include <ctime>
#include <cmath>
#include <vector>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include "blas1.h"

using namespace blas1;

int main() {
  constexpr int n = 100000;
  constexpr int m = 10000;
  double v1[n] __attribute__((aligned(32)));
  double v2[n] __attribute__((aligned(32)));

  std::mt19937 rng;
  rng.seed(33);
  std::uniform_real_distribution<double> dist(0.0,1.0);
  for (size_t j = 0; j < n; ++j) {
    v1[j] = dist(rng);
    v2[j] = dist(rng);
  }

  const auto tot2 = dot1(n, v1, v2);
  const auto timeFunction = [&] (auto fun, const auto name) {
    auto tot = 0.0;
    const auto startTime = static_cast<double>(clock());
    for (size_t i = 0; i < m; ++i) {
      const auto t = fun(n, v1, v2);
      tot += t;
    }
    tot = fun(n, v1, v2);
    const auto timeSpan = (clock() - startTime) / CLOCKS_PER_SEC;
    std::cout << std::setw(6) << name << ":\t" << std::setw(20) << timeSpan;
    std::cout << "\t" << std::setw(20) << tot << ",\t" << std::setw(20) <<  std::fabs(tot2 - tot) << std::endl;
  };

  std::cout << std::setprecision(17);
  timeFunction(&dot1, "dot1");
  timeFunction(&dot2, "dot2");
  timeFunction(&dot3, "dot3");
  timeFunction(&dot3prim, "dot3p");
  timeFunction(&dot4, "dot4");
  timeFunction(&dot5, "dot5");
  timeFunction(&dot6, "dot6");
  timeFunction(&dot7, "dot7");
  timeFunction(&dot8, "dot8");
  timeFunction(&dot9, "dot9");

  return 0;
}
