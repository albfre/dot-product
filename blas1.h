#ifndef BLAS_1_H
#define BLAS_1_H

#include <vector>
#include <immintrin.h>
#include <cassert>

namespace blas1 {
  double dot1(const size_t n, const double* a, const double* b) {
    double t = 0.0;
//#pragma clang loop vectorize(enable) interleave(enable)
    for (const double* const e = a + n; a != e; ++a, ++b) {
      t += *a * *b;
    }
    return t;
  }

  double dot2(const size_t n, const double* a, const double* b) {
    double t = 0.0;
//#pragma clang loop vectorize(enable) interleave(enable)
    for (size_t i = 0; i < n; ++i) {
      t += a[i] * b[i];
    }
    return t;
  }

  double dot3(const size_t n, const double* a, const double* b) {
    double t1 = 0.0;
    double t2 = 0.0;
    double t3 = 0.0;
    double t4 = 0.0;
    size_t i = 0;
    for (; i + 3 < n; i += 4) {
      t1 += a[i] * b[i];
      t2 += a[i + 1] * b[i + 1];
      t3 += a[i + 2] * b[i + 2];
      t4 += a[i + 3] * b[i + 3];
    }
    for (; i < n; ++i) {
      t1 += a[i] * b[i];
    }
    return t1 + t2 + t3 + t4;
  }

  double dot3prim(const size_t n, const double* a, const double* b) {
    std::vector< double > buf(4);
    size_t i = 0;
    for (; i + 3 < n; i += 4) {
      buf[0] += a[i] * b[i];
      buf[1] += a[i + 1] * b[i + 1];
      buf[2] += a[i + 2] * b[i + 2];
      buf[3] += a[i + 3] * b[i + 3];
    }
    for (; i < n; ++i) {
      buf[0] += a[i] * b[i];
    }
    return buf[0] + buf[1] + buf[2] + buf[3];
  }

  double dot4(const size_t n, const double* a, const double* b) {
    double t1 = 0.0;
    double t2 = 0.0;
    double t3 = 0.0;
    double t4 = 0.0;
    double t5 = 0.0;
    double t6 = 0.0;
    double t7 = 0.0;
    double t8 = 0.0;
    size_t i = 0;
    for (; i + 7 < n; i += 8) {
      t1 += a[i] * b[i];
      t2 += a[i + 1] * b[i + 1];
      t3 += a[i + 2] * b[i + 2];
      t4 += a[i + 3] * b[i + 3];
      t5 += a[i + 4] * b[i + 4];
      t6 += a[i + 5] * b[i + 5];
      t7 += a[i + 6] * b[i + 6];
      t8 += a[i + 7] * b[i + 7];
    }
    for (; i < n; ++i) {
      t1 += a[i] * b[i];
    }
    return t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8;
  }

  template<class T>
  bool isAligned(const T* address, size_t bytes)
  {
    return (reinterpret_cast<size_t>(address) % bytes == 0);
  }

  double dot5(const size_t n, const double* x, const double* y) {
    assert(isAligned(x, 16));
    assert(isAligned(y, 16));
    size_t nb = n / 2;
    size_t nl = n % 2;

    __m128d x12, y12, t1;
    double t1_[2];

    t1 = _mm_setzero_pd();
    for (size_t i = 0; i < nb; ++i) {
      x12 = _mm_load_pd(x);
      y12 = _mm_load_pd(y);
      x12 = x12 * y12;
      t1 = t1 + x12;

      x += 2;
      y += 2;
    }
    _mm_store_pd(t1_, t1);

    double result = t1_[0] + t1_[1];

    for (size_t i = 0; i < nl; ++i) {
      result += x[i] * y[i];
    }
    return result;
  }

  double dot6(const size_t n, const double* x, const double* y) {
    assert(isAligned(x, 32));
    assert(isAligned(y, 32));
    const size_t nb = n / 4;
    const size_t nl = n % 4;

    __m256d x12, y12, t1;
    double t1_[4];

    t1 = _mm256_setzero_pd();
    for (size_t i = 0; i < nb; ++i) {
      x12 = _mm256_load_pd(x);
      y12 = _mm256_load_pd(y);
      x12 = x12 * y12;
      t1 = t1 + x12;

      x += 4;
      y += 4;
    }
    _mm256_store_pd(t1_, t1);

    double result = t1_[0] + t1_[1] + t1_[2] + t1_[3];

    for (size_t i = 0; i < nl; ++i) {
      result += x[i] * y[i];
    }
    return result;
  }

  double dot7(const size_t n, const double* a, const double* b) {
    assert(isAligned(a, 32));
    assert(isAligned(b, 32));
    __m256 t1 = _mm256_setzero_pd();
    const size_t nb = n / 4;
    const size_t nl = n % 4;
    for (size_t i = 0; i < nb; ++i) {
      t1 = _mm256_fmadd_pd(_mm256_loadu_pd(a),
                           _mm256_loadu_pd(b),
                           t1);
      a += 4;
      b += 4;
    }
    double buffer[4];
    _mm256_storeu_pd(buffer, t1);
    double t = buffer[0] + buffer[1] + buffer[2] + buffer[3];
    for (size_t i = 0; i < nl; ++i) {
      t += a[i] * b[i];
    }
    return t;
  }

  double dot8(const size_t n, const double* a, const double* b) {
    assert(isAligned(a, 32));
    assert(isAligned(b, 32));
    __m256 t1 = _mm256_setzero_pd();
    __m256 t2 = _mm256_setzero_pd();
    const size_t nb = n / 8;
    const size_t nl = n % 8;
    for (size_t i = 0; i < nb; ++i) {
      t1 = _mm256_fmadd_pd(_mm256_loadu_pd(a),
                           _mm256_loadu_pd(b),
                           t1);
      t2 = _mm256_fmadd_pd(_mm256_loadu_pd(a + 4),
                           _mm256_loadu_pd(b + 4),
                           t2);
      a += 8;
      b += 8;
    }
    double buffer[4];
    _mm256_storeu_pd(buffer, t1);
    double t = buffer[0] + buffer[1] + buffer[2] + buffer[3];
    _mm256_storeu_pd(buffer, t2);
    t += buffer[0] + buffer[1] + buffer[2] + buffer[3];
    for (size_t i = 0; i < nl; ++i) {
      t += a[i] * b[i];
    }
    return t;
  }

  double dot9(const size_t n, const double* a, const double* b) {
    assert(isAligned(a, 32));
    assert(isAligned(b, 32));
    __m256 t1 = _mm256_setzero_pd();
    __m256 t2 = _mm256_setzero_pd();
    __m256 t3 = _mm256_setzero_pd();
    __m256 t4 = _mm256_setzero_pd();
    const size_t nb = n / 16;
    const size_t nl = n % 16;
    for (size_t i = 0; i < nb; ++i) {
      t1 = _mm256_fmadd_pd(_mm256_loadu_pd(a),
                           _mm256_loadu_pd(b),
                           t1);
      t2 = _mm256_fmadd_pd(_mm256_loadu_pd(a + 4),
                           _mm256_loadu_pd(b + 4),
                           t2);
      t3 = _mm256_fmadd_pd(_mm256_loadu_pd(a + 8),
                           _mm256_loadu_pd(b + 8),
                           t3);
      t4 = _mm256_fmadd_pd(_mm256_loadu_pd(a + 12),
                           _mm256_loadu_pd(b + 12),
                           t4);
      a += 16;
      b += 16;
    }
    double buffer[4];
    _mm256_storeu_pd(buffer, t1);
    double t = buffer[0] + buffer[1] + buffer[2] + buffer[3];
    _mm256_storeu_pd(buffer, t2);
    t += buffer[0] + buffer[1] + buffer[2] + buffer[3];
    _mm256_storeu_pd(buffer, t3);
    t += buffer[0] + buffer[1] + buffer[2] + buffer[3];
    _mm256_storeu_pd(buffer, t4);
    t += buffer[0] + buffer[1] + buffer[2] + buffer[3];

    for (size_t i = 0; i < nl; ++i) {
      t += a[i] * b[i];
    }
    return t;
  }
}

#endif // BLAS_1_H
